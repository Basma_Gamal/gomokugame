import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;


public class Board extends Canvas implements MouseListener{



	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		
		Board b =new Board();
		
        //create a new frame to which we will add a canvas
        JFrame aFrame = new JFrame();
        aFrame.setSize(300, 300);
        
        //add the canvas
        aFrame.add(b);
        
        aFrame.setVisible(true);

		
	}
	*/
	/*
	int cells[][]= new int[16][16];

	static final int EMPTY = 0;
	static final int CROSS = 1;
	static final int ZERO = 2;
	static final int BORDER = 3;
	*/

	Point mousePoint = new Point();



	public Point getMousePoint() {
		return mousePoint;
	}

	public void setMousePoint(Point mousePoint) {
		this.mousePoint = mousePoint;
	}

	/**
	 * Create the application.
	 */
	public Board() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//setSize(200, 200);
        setBackground(new Color(87, 27, 126));
        
		 addMouseListener(this);

		
		/*
		int rowsCol=16,  heightWidth=20;
		Dimension dim = new Dimension(rowsCol * heightWidth + 1, rowsCol * heightWidth+ 1);
		Color border= new Color(66, 125, 26);
		Canvas can = new Canvas();
		can.setBackground(new Color(87, 27, 126));
		
		
		g = can.getGraphics();
		int width = rowsCol * heightWidth;
		int height = rowsCol * heightWidth;

		// Draw grid
		//g.setColor(border);
		/*
		for (int i = rowsCol; i >= 0; i--)
			g.drawLine(0 + i * heightWidth, 0, 0 + i * heightWidth, 0 + height);
		for (int j = rowsCol; j >= 0; j--)
			g.drawLine(0, 0 + j * heightWidth, 0 + width, 0 + j * heightWidth);
        
		g.drawString("Hello", 200, 200);
		frame.add(can);
		*/
		
	}
	
	
	  public void paint(Graphics g){
		     
		  // Draw grid
		  g.setColor(new Color(66, 125, 26));
	       /* g.drawLine(30, 30, 80, 80);
	        g.drawRect(20, 150, 100, 100);
	        g.fillRect(20, 150, 100, 100);
	        g.fillOval(150, 20, 100, 100); 
	        Image img1 = Toolkit.getDefaultToolkit().getImage("sky.jpg"); 
	        g.drawImage(img1, 140, 140, this); */
	       // g.drawString("Hello", 200, 200);
	        
	        for (int i = 16; i >= 0; i--)
	        {
				g.drawLine(0 + i * 30, 0, 0 + i * 30, 0 + 16*30);
	        }
			for (int j = 16; j >= 0; j--)
			{
				g.drawLine(0, 0 + j * 30, 0 + 16*30, 0 + j * 30);
			}
			
			
			
			// Draw pieces
			g.setColor(Color.WHITE);
			for (int i = 15; i >= 0; i--)
				for (int j = 15; j >= 0; j--)
				{
					if(mousePoint.getX()!=0 & mousePoint.getY()!=0)
					{
						g.setColor(Color.BLACK);
					//g.fillOval(i * 30 + 4 + 0, j * 30 + 4 + 0,30 -8, 30 - 8);
						//if(mousePoint.getX()< i * 30 + 4  && mousePoint.getX() > i-1 * 30 + 4  && mousePoint.getY()< j * 30 + 4  && mousePoint.getY() > j-1 * 30 + 4 )
							g.fillOval(mousePoint.getX(), mousePoint.getY() ,30 -8, 30 - 8);
						
					}
				}
			/*
			for (int i = 15; i >= 0; i--)
				for (int j = 15; j >= 0; j--)
					switch (cells[i][j]) {
					case EMPTY:
						// int eval = evaluate( i,j );
						// g.drawString( ""+eval, i*cellWidth + 1, j*cellHeight -9
						// );
						break;

					case CROSS:
						g.drawLine(i * 30 + 2 + 0, j * 30 + 2 + 0,
								(i + 1) * 30 - 2 + 0, (j + 1) * 30
										- 2 + 0);
						g.drawLine((i + 1) * 30 - 2 + 0, j * 30 + 2
								+ 0, i * 30 + 2 + 0, (j + 1) * 30
								- 2 + 0);
						break;

					case ZERO:
						g.drawOval(i * 30 + 2 + 0, j * 30 + 30 + 0,
								30 - 4, 30 - 4);
						break;

					default:
						// error!
						g.fillRect(i * 30 + 1 + 0, j * 30 + 1 + 0,
								30 - 1, 30 - 1);
					}
	        */
	    }

	@Override
	public void mouseClicked(MouseEvent event) {
		// TODO Auto-generated method stub

		 mousePoint.setX(event.getX());
		 mousePoint.setY(event.getY());
		repaint();
		
		//System.out.println("mouseClicked x "+ mousePoint.getX() +" y "+ mousePoint.getY());
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	


}
