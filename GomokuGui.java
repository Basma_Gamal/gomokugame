import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import java.awt.GridLayout;

import javax.swing.JSeparator;
import javax.swing.border.Border;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JSplitPane;
import javax.swing.JLabel;


public class GomokuGui {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GomokuGui window = new GomokuGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		

		
	}

	/**
	 * Create the application.
	 */
	public GomokuGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(300, 100, 570, 545);
		frame.setTitle("Gomoku");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnMainMenu = new JMenu("Main Menu");
		menuBar.add(mnMainMenu);
		
		JMenuItem mntmNewGame = new JMenuItem("New Game");
		mnMainMenu.add(mntmNewGame);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnMainMenu.add(mntmExit);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmHelpContents = new JMenuItem("Help Contents");
		mnHelp.add(mntmHelpContents);
		
		JSeparator separator = new JSeparator();
		mnHelp.add(separator);
		
		JMenuItem menuItem = new JMenuItem("About Gomoku");
		mnHelp.add(menuItem);
		
		/*
		GoMokuBoard g= new GoMokuBoard(16,16,20,20);
		frame.add(g);
		*/
		
		mntmExit.addActionListener(new Action(mntmExit));
		mntmHelpContents.addActionListener(new Action(mntmHelpContents));
		menuItem.addActionListener(new Action(menuItem));
		
		
		mntmNewGame.addActionListener(new Action(mntmNewGame,frame));

		/**/
		Board b =new Board();
		
		//frame.add(b);
	
		
	   

		
		// basma board made by buttons.
		
		/*
		
		 frame.setLayout(new GridLayout(16,16)); //set layout
		  JButton[][] grid;
		 Border border = BorderFactory.createLineBorder(new Color(138, 65, 23));
		 ActionListener act = new ActionListener()
         {
        	   
				@Override
				public void actionPerformed(ActionEvent event) {
					// TODO Auto-generated method stub
					 ImageIcon image =new ImageIcon("H://asmaa//7thYear//1st Semester//Software enginering 1//gomoku code form others//WhiteStone.png");
					((AbstractButton) event.getSource()).setBackground(Color.WHITE);
					((AbstractButton) event.getSource()).setIcon(image);
					((AbstractButton) event.getSource()).setEnabled(false);
					 
					
				}
        	};
         grid=new JButton[16][16]; //allocate the size of grid
         for(int y=0; y<16; y++){
                 for(int x=0; x<16; x++){
                         grid[x][y]=new JButton(" "); //creates new button 
                         grid[x][y].setBackground(new Color(126, 53, 23));
                        // grid[x][y].setBorder(border);
                         grid[x][y].addActionListener(act);
                         frame.add(grid[x][y]); //adds button to grid
                 }
         }
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         frame.pack(); //sets appropriate size for frame
        
		*/
	}

}
