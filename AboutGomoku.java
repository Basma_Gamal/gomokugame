import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;


public class AboutGomoku {

	private JFrame aboutFrame;

	// to run as main to see it for now
/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AboutGomoku window = new AboutGomoku();
					window.aboutFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	*/
	/**
	 * Create the application.
	 */
	public AboutGomoku() {
		aboutInitialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void aboutInitialize() {
		aboutFrame = new JFrame();
		aboutFrame.setBounds(100, 50, 300, 200);
		aboutFrame.setTitle("About Gomoku");
		aboutFrame.setVisible(true);
		
		String about="   Version: Gomoku Release 1 \n" +
		
                 "   Build id: 20130919-0819\n "  +
                 "  (c) Copyright Gomoku Team  All rights reserved. \n";
		JTextPane textPane = new JTextPane();
		textPane.setText(about);
		textPane.setContentType("text/plain");
		textPane.setEditable(false);
		aboutFrame.getContentPane().add(textPane,BorderLayout.CENTER);
	
	}

}
